export class Calculator {
    multiply(numberA: number, numberB: number): number {
        return numberA * numberB;
    }
    divide(numberA: number, numberB: number): number {

        // Refactorización nueva
        if(numberB === 0) {
            return null;
        }
        return numberA / numberB;
    }
}
