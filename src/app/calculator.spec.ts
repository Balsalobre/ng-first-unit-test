import { Calculator } from './calculator';

describe('Test for Calculator', () => {

  describe('Test for multiply', () => {

    it('should return nine', () => {
      // Arrange --> Preparar
      let calculator = new Calculator();
      // Act --> Actuar
      let result = calculator.multiply(3, 3);
      // Assert
      // Jasmine nos entrega un suite de funciones para hacer
      // comprobaciones y demás
      expect(result).toEqual(9);
    });

    it('should return four', ()=> {
      // Arrange --> Preparar
      let calculator = new Calculator();
      // Act --> Actuar
      let result = calculator.multiply(1, 4);
      // Assert --> Verificar
      // Jasmine nos entrega un suite de funciones para hacer
      // comprobaciones y demás
      expect(result).toEqual(4);
    });

  });

});